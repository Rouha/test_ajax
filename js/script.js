$(document).ready(function(e){
    $("#searchid").keyup(function(){
        var text = $(this).val();

        var key = e.which;
        if(key == 13)  // the enter key code
        {
            e.preventDefault();
            $.ajax({
                type: "POST",
                url: "controls/fetch.php",
                data: 'text='+text,
                dataType: "html",
                async: false,
                success: function(text){
                    $('#List table tbody').html(text);
                }

            });
            $("option:selected").prop("selected", false);

        }

        $.ajax({
            type: "POST",
            url: "controls/fetch.php",
            data: 'text='+text,
            dataType: "html",
            async: false,
            success: function(text){
                $('#List table tbody').html(text);
            }

        });
    });
});

$('form.ajax').submit(function (e) {
    e.preventDefault();

    $.ajax({
        method: "post",
        data: $(e.target).serialize(),
        url: "controls/insert.php"
    }).done(function(data){

        console.log(data);

        var title = data['new_data']['title'];
        var date = data['new_data']['date'];
        var time = data['new_data']['time'];
        var id = data['row_id'];

        if (title == '' || date == '') {

            $('#Error').show().html(data['status']).removeClass().addClass('red');

        } else {

            $('#List table.list tbody').append(

                '<tr id="' + id + '">' +

                '<td class="editable-col" contenteditable="true" oldVal="' + title +'">' + title + '</td>' +
                '<td>' + date + ' - ' + time + '</td>' +
                '<td><i class="fa fa-times delete" aria-hidden="true" style="cursor: pointer;"></i></td>' +

                '</tr>'

            );

            $('#Error').show().html(data['status']).removeClass().addClass('green');

        }

    });

    $('form.ajax')[0].reset();

});

$(document).ready(function(){
    $(document).on('keypress', '.editable-col', function (e) {
        if(e.which === 13){
            data = {};
            data['title'] = $(this).text();
            data['id'] = $(this).parent('tr').attr('id');
            if($(this).attr('oldVal') === data['title'])
                return false;

            $(this).blur().next().focus();

            $.ajax({

                type: "POST",
                url: "controls/update.php",
                data: data
            }).done(function (data) {

                $('#Error').show().html(data['status']).removeClass().addClass('orange');

            });
        }
    });
});

$(document).ready(function(){
    $(document).on('focusout', '.editable-col', function() {
        data = {};
        data['title'] = $(this).text();
        data['id'] = $(this).parent('tr').attr('id');
        if($(this).attr('oldVal') === data['title'])
            return false;

        $.ajax({

            type: "POST",
            url: "controls/update.php",
            data: data
        }).done(function (data) {

            $('#Error').show().html(data['status']).removeClass().addClass('orange');

        });

    });

    $(document).on('click', '.delete', function() {
        if (confirm('Smazat položku?')) {
            data = {};
            data['id'] = $(this).parents('tr').attr('id');
            $.ajax({

                type: "POST",
                url: "controls/delete.php",
                data: data
            }).done(function (data) {

                $('#Error').show().html(data['status']).removeClass().addClass('black');

            });

            document.getElementById(data['id']).remove();

        }

    });


});