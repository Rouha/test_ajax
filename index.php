<!DOCTYPE html>
<html>

<title>Poznámky</title>

<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1">

<link rel="stylesheet" href="styles/bootstrap.min.css">
<link rel="stylesheet" href="styles/dist/compiled_styles.min.css">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">

<script src="js/jquery.min.js"></script>
<script src="js/bootstrap.min.js"></script>
<script src="js/date.js"></script>

<body>

    <div id="Calendar">

        <div id="Controls">

            <div id="Date">

                <span id="date_time"></span>
                <script type="text/javascript">window.onload = date_time('date_time');</script>

            </div>

            <div id="SearchBox">

                    <form role="search">
                        <div class="input-group">
                            <input type="text" class="form-control search" placeholder="Hledat" name="keyword" id="searchid" >
                            <div class="input-group-btn">
                                <button class="btn btn-default" type="submit"><i class="fa fa-search fa-lg" aria-hidden="true"></i></button>
                            </div>
                        </div>
                        <div id="results"></div>
                    </form>

            </div>

        </div>

        <div class="clearfix"></div>

        <div id="Error"></div>

        <div id="AddItem">

            <form class="form-inline ajax">
                <div class="form-group">
                    <label for="title">Co:</label>
                    <input type="text" class="form-control" id="title" name="title">
                </div>
                <div class="form-group">
                    <label for="date">Datum:</label>
                    <input type="date" class="form-control" id="date" name="date">
                </div>
                <div class="form-group">
                    <label for="time">Čas:</label>
                    <input type="time" class="form-control" id="time" name="time">
                </div>
                <button type="submit" class="btn btn-default" name="send">Zapsat</button>
            </form>

        </div>

        <div id="List">

            <?php include 'controls/select.php'?>

            <table class="table table-hover list">
                <thead>
                    <tr class="table-head">
                        <th style="width:50%;">Událost</th>
                        <th>Kdy</th>
                        <th>Smazat</th>
                    </tr>
                </thead>

                    <tbody id="_editable_table">

                    <?php foreach($rows as $row ) {?>

                        <tr id="<?php echo $row['id'];?>">
                            <td class="editable-col" contenteditable="true" oldVal ="<?php echo $row['title'];?>"><?php echo $row['title'];?></td>
                            <td><?php echo $row['date'] . ' - ' . $row['time'];?></td>
                            <td><i class="fa fa-times delete" aria-hidden="true" style="cursor: pointer;"></i></td>
                        </tr>

                    <?php } ?>

                    </tbody>

            </table>

        </div>

    </div>

    <script src="js/script.js"></script>

</body>
</html>