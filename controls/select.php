<?php

    include 'db/db.php';

    $sql = "SELECT id, title, date, time FROM items";

    $statement = $conn->prepare($sql);

    $statement->execute();

    $rows = $statement->fetchAll(PDO::FETCH_ASSOC);