<?php

    include '../db/db.php';

    $id = $_POST['id'];

    $stmt = $conn->prepare("DELETE FROM items WHERE id = $id");

    $stmt->execute();

    $conn = null;

    $return = [];

    $return['status'] = 'Událost smazána!';

    header('Content-type: application/json');
    echo json_encode($return, JSON_UNESCAPED_UNICODE);