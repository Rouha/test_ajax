<?php

    include "../db/db.php";

    $text = $_POST['text'];

    $sql = "SELECT * FROM items where title LIKE '%$text%' OR date LIKE '%$text%'";

    $statement = $conn->prepare($sql);

    $statement->execute();

    $rows = $statement->fetchAll(PDO::FETCH_ASSOC);

?>
<?php
    foreach($rows as $row ) {
?>

    <tr id="<?php echo $row['id'];?>">
        <td class="editable-col" contenteditable="true" oldVal ="<?php echo $row['title'];?>"><?php echo $row['title'];?></td>
        <td><?php echo $row['date'] . ' - ' . $row['time'];?></td>
        <td><i class="fa fa-times delete" aria-hidden="true" style="cursor: pointer;"></i></td>
    </tr>

<?php } ?>