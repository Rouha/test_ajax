<?php

    include '../db/db.php';

    $return = [];

    if(!empty($_POST['title'] && !empty($_POST['date']))) {

        $title = $_POST['title'];
        $date = $_POST['date'];
        $time = $_POST['time'];

        $stmt = $conn->prepare("INSERT INTO items (title, date, time) 
                                VALUES (:title, :date, :time)");
        $stmt->bindParam(':title', $title);
        $stmt->bindParam(':date', $date);
        $stmt->bindParam(':time', $time);

        $stmt->execute();

        $last_row_id = $conn->lastInsertId();

        $conn = null;

        $return['row_id'] = $last_row_id;

        $return['status'] = 'Událost přidána.';

        $return['new_data'] = [];
        $return['new_data'] = $_POST;

    } else {

        if (empty($_POST['title'])) {
            $return['title'] = 'Chybí název';
        }
        if (empty($_POST['date'])) {
            $return['date'] = 'Chybí datum';
        }

        $return['new_data'] = [];
        $return['new_data'] = $_POST;

        $return['status'] = 'Chyba, zadejte událost i datum!';

    }

    header('Content-type: application/json');
    echo json_encode($return, JSON_UNESCAPED_UNICODE);